<?php
use PHPMailer\PHPMailer\PHPMailer;
require 'vendor/autoload.php';

try {
	include("admin/config.php");
	include("admin/functions.php");

//	sendEmail($pdo,'makuno.biz@gmail.com','Subject Test','Emailing works <a href="https://google.com">gooogle</a>');

	$emails = $pdo->prepare('SELECT * FROM send_emails WHERE sent = false ');
	$emails->execute();
	$unsentEmails = $emails->fetchAll(PDO::FETCH_ASSOC);

	foreach ($unsentEmails as $email) {
		$to_email = $email['email'];
		$subject = $email['subject'];
		$message = $email['message'];
		$headers = 'From: info.garifast@gmail.com';

		$mail = new PHPMailer(true);
		$mail->isSMTP();
		$mail->SMTPDebug = 2;
		$mail->Host = 'smtp.googlemail.com';
		$mail->Port = 587;
		$mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;

		$mail->SMTPAuth = true;
		$mail->Username = 'info.garifast@gmail.com';
		$mail->Password = 'garifast2020';

		$mail->setFrom('info.garifast@gmail.com', 'Garifast Support');
		$mail->addReplyTo('info.garifast@gmail.com', 'Garifast Support');
		$mail->addAddress($to_email, 'Customer');
		$mail->isHTML(true);
		$mail->Subject = $subject;
		$mail->Body = $message;

		if (!$mail->send()) {
			echo 'Mailer Error: ' . $mail->ErrorInfo;
			continue;
		} else {
			echo 'The email message was sent.';
		}

		//update db
		$update = $pdo->prepare("UPDATE send_emails SET sent = true where id='".$email['id']."' ");
		$update->execute();
	}
} catch (Exception $exception) {
	echo($exception->getMessage());
}
?>